---
title: Home
_fieldset: home
_template: default
intro1: |
  <p class="lead">
  	<span class="hilite">First, we partner with experienced entrepreneurs around the world.</span> Then we immerse them in the power of Coca-Cola -- our relationships, resources, and reach -- <em><strong data-redactor-tag="strong">before</strong></em> they create a startup. Together we focus on big problems lots of people have. Using lean startup methods, they grow the startup with Coca-Cola as the lead backer. Once the business model is proven, Coca-Cola becomes a minority shareholder. We collaborate from the very start to create more speed, more scale, and more impact.</p>
intro2: |
  <p class="lead"><span class="hilite">This model creates a win-win for everyone.</span> Founders are given an unfair advantage through the power of Coca-Cola and the opportunity to do what most can only dream about. Coca-Cola gets early access to new, fast-growing markets and proven growth opportunities for our business. We've been partnering entrepreneurs since our inception and strongly believe in Robert Woodruff's words when we said “there is no limit to what a man (or woman) can do or how far he can go if he doesn't mind who gets the credit."</p>
resources:
  - 
    heading: People
    copy: We have more than 700,000 system employees globally
  - 
    heading: Expertise
    copy: We have expertise in marketing, supply chain, food industry, music industry, advertising, agriculture, mobile, tax, food science, water sustainability, trademark law and more
  - 
    heading: Equipment
    copy: We have more than 15 million pieces of cold-drink equipment system-wide around the world
reach:
  - 
    heading: Operations
    copy: |
      |
            We sell our products in more than 200 countries
  - 
    heading: Fans
    copy: |
      |
            We have nearly 90 million Facebook® fans, and counting
  - 
    heading: Distribution
    copy: |
      |
            We have more trucks on the road than the big three delivery services combined
relationships:
  - 
    heading: Retailers
    copy: |
      |
            We interact with more than 24 million customer outlets around the world and have partnerships with the biggest regional and global retailers
  - 
    heading: Leaders
    copy: |
      |
            We have relationships with global decision makers, NGOs, and celebrities
  - 
    heading: Media
    copy: |
      |
            We have relationships with large and small media outlets
  - 
    heading: Partnerships
    copy: |
      |
            We partner with some of the world’s most celebrated events like the Olympics and the FIFA World Cup
  - 
    heading: Vendors
    copy: |
      |
            Our scale means an effective supply chain and purchasing power with vendors.
qa:
  - 
    question: What is the Coca-Cola Founders platform?
    answer1: "<p>Launched in 2013, the Coca-Cola Founders platform is a new model for creating seed-stage startups. We partner with experienced entrepreneurs around the world and give them access to Coca-Cola's relationships, resources and reach before they develop their next startup.</p>"
    answer2: '<p>This creates a win-win. Coca-Cola gets early access to new markets with proven growth opportunities for our business, while our founders experience an unfair advantage that most can only dream about.</p>'
  - 
    question: How does it work?
    answer1: "<p>We search for experienced entrepreneurs in startup communities around the world. Next we invite the founders to join our Co-Founder Network. Once they join, each Co-Founder team creates a new legal entity (they own 100% of their startup from day 1). Then we open up Coke's assets – our relationships, resources and reach – and connect them to a local, senior manager inside our business as an advisor.</p>"
    answer2: '<p>This structure creates an “inside" connection to Coke while maintaining the flexibility and speed required to build a startup. Once the team validates their business model and the business is ready to scale, we convert our investment to a minority share of equity based on market valuation.</p>'
  - 
    question: Is this just another accelerator?
    answer1: "<p>No. Most accelerators conduct three to six month programs and focus on financial investment and mentoring. Our model is very different. We invest in founders first, before they have a startup or even an idea. We don't dictate the problem they should solve or the industry they should focus on, and we give them the time and attention required to truly validate the problem, design the best product/market fit and find a business model to create a sustainable company.</p>"
    answer2: "<p>And, because they're part of our global Co-Founder Network, they're part of a larger team of founders all trying to do the same thing around the world. This makes it easier to scale, and makes the journey a little less lonely.</p>"
  - 
    question: Why is Coca-Cola doing this?
    answer1: '<p>Coca-Cola has been empowering entrepreneurs since 1899 when the first bottling agreements were established. As Americans became more mobile, we saw an opportunity to leverage new technologies and business models to capture growth. We opened up our assets – relationships, resources and reach – to independent entrepreneurs across the US granting them the right to bottle and sell Coca-Cola®. These partnerships formed the foundation of our global business today, and have built one of the most loved and valued brands in the world.</p>'
    answer2: |
      <p>The Coca-Cola Founders platform lets us empower entrepreneurs once again, in a way that only Coca-Cola can, while opening up entirely new markets that can fuel Coca-Cola's growth.</p><p>“There is no limit to what a man can do or how far he can go if he doesn't mind who gets the credit."</p><p>Robert Woodruff<br>Coca-Cola President, 1923-1954</p>
  - 
    question: What’s in it for Coca-Cola?
    answer1: "<p>The Founders platform has the potential to benefit Coca-Cola in multiple ways. When a Co-Founder team creates the next billion-dollar startup, it's a win-win for everyone. The startup's success is a win for Coca-Cola as an investor, gives us access to new markets, and lets us leverage our assets in new ways.</p>"
    answer2: "<p>If we can commercialize the startup's product or service within Coca-Cola, it can drive topline growth via product or service sales, as well as bottom line growth via operational efficiencies. The potential benefits for Coca-Cola are massive, but can only be realized when our Founders understand how we operate, the complexities of our business, and the opportunities we see in our markets.</p>"
  - 
    question: What can the Founders platform offer entrepreneurs that they can’t get somewhere else?
    answer1: '<p>Our founders have an unfair advantage in the relationships, reach and resources that Coca-Cola provides. It takes a lot more than money to build a business. Founders routinely leverage our equipment, event partnerships, media relationships, distribution channels, and expertise in multiple areas such as marketing, tax, supply chain, and more to test, build and scale their ideas. Having untethered access to this wealth of assets, backed by one of the most trusted brand names in the world, is something that most founders can only dream about.</p>'
    answer2: |
      <p>Here's an example. One of our Founder teams was in search of retail cooler space for their product. They approached store owners as “the founders of a new startup" and were politely turned away. They later revisited those same stores, but brought Coca-Cola reps with them, and were immediately granted shelf space. The power of relationships goes a long way. When scaled across 200+ countries, that's the power of Coca-Cola.</p>
  - 
    question: What types of founders are you looking for?
    answer1: "<p>We don't look for first-time founders or people with dreams of running a startup. We partner with experienced entrepreneurs. They've been there and done that. They know the challenges that come with searching for a problem, designing a product, building a team, raising funding, marketing products, and developing customers. They're ready to build something with the kind of impact that can only be achieved by partnering with Coke. Sometimes it comes down to timing.</p>"
    answer2: '<p>Many of our current founders came onboard after a huge failure or significant exit.</p><p>In addition, our founders are experts in the Lean Startup Method. Lean tools and principles – emphasizing low spend &amp; high speed – are core to the way we approach building a startup. <a href="http://founders.dev/#co-founders">Get to know our founders</a>.</p>'
  - 
    question: How do you find your founders?
    answer1: "<p>We don't accept applications. We hand pick every founder. We go deep into the startup community to search for the right fit, and we tap into our existing Co-Founder Network for references. Our goal is to build a global network.</p>"
    answer2: '<p>Our goal is to build a global network, and our reach inside the startup community expands with each new Co-Founder team we add, fueling the growth of our network.</p>'
  - 
    question: What kind of startups are you building through the platform?
    answer1: "<p>We start with problems – big problems that a lot of people have. Partnering with Coca-Cola makes the difficult process of building a sustainable business around these problems a little bit easier. Because of our breadth and scale, we know we'll be affected by these problems – and their solutions – now or in the future.</p>"
    answer2: "<p>And while many problems cross borders, it's hard to solve global problems. So we start locally. From Day 1, the founders are connected to a local Coca-Cola advisor. All of the teams focus on solving a problem in their neighborhood, then their city, then their country – with the intent to scale globally.</p><section></section>"
contact: '<p>You can reach the Coca-Cola Founders platform <a href="mailto:foundersplatform@coca-cola.com">here</a>.</p>'
startups:
  - /startups/home-eat-home
  - /startups/tobuy
  - /startups/truu-mobile
  - /startups/winnin
  - /startups/wonolo
  - /startups/vidati
  - /startups/ihydrate
  - /startups/stealth-mode
video: ' <iframe src="//player.vimeo.com/video/111142243?title=0&byline=0&portrait=0&color=cc0000" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>'
news:
  - /news/coke-reveals-details-of-its-tech-start-up-network
  - /news/discovering-the-next-google-will-put-the-fizz-back-in-coke
  - /news/meet-the-coca-cola-co-founders
  - /news/the-25-best-global-companies-to-work-for
  - /news/how-it-works-funded-by-coca-cola-startup-home-home-eat
---






