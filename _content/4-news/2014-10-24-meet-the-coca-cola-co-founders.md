---
title: 'Meet the Coca-Cola Co-founders: A Global Network Of Entrepreneurs Partnering With Coke to Build High-Growth Startups'
news_link: http://www.coca-colacompany.com/stories/meet-the-coca-cola-co-founders-a-global-network-of-entrepreneurs-partnering-with-coke-to-build-high-growth-startups
news_img: ""
news_file: ""
news_desc: '<p><br></p>'
news_pub_logo: /assets/images/tc-logo.svg
categories: press
cats: press
---

