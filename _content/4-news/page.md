---
_fieldset: news
title: News
_template: news
_default_folder_template: news
---
This is the News page.

News listing.

<ul>
    {{ entries:listing }}
        <li>
            <a href="{{ url }}">{{ title }}</a>
        </li>
    {{ /entries:listing }}
</ul>