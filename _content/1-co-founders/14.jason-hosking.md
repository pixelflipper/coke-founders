---
title: Jason Hosking
photo: /assets/images/jason_hosking.png
twitter: @hoskioski
linkedin: http://au.linkedin.com/in/hoskingjason
startup: /startups/ihydrate
quote: "It’s better to regret something you do, than something you don't."
startup2:
  - /startups/nudgefx
  - /startups/vending-analytics
---
<p>Jason has a passion for turning ideas into reality, and brings a wealth of experience from working with all types of businesses – be it early stage startups, large multinationals or anything in between. Jason started his career in a boutique media production house in Sydney and after completing his MBA joined Accenture’s Strategy Practice, focusing on transformation initiatives for Telco, Media and Entertainment clients. In between, Jason helped raise VC funding for a business with breakthrough technology in 3D sound. Jason then joined the internal strategy division of Macquarie Group, offering strategic advice and expertise across all of Macquarie’s businesses and divisions. He also relocated to New York to establish the North American office, advising Macquarie’s businesses in its priority growth region. Recently, Jason returned to Australia and joined Rocket Internet – a leading Venture Builder – as part of the management team of Zanui and also worked with Alive Mobile as Head of New Ventures, where he was prototyping a disruptive innovation for the recruitment industry.</p><p>Growing up Jason was lucky enough to take time off school to travel with camera crews filming stories for shows like Beyond 2000 and Invention on the Discovery Channel. It was during this time that his passion for new ideas was born. Whether it was riding the first winged skateboard, learning how champagne was invented or visiting Leonardo da Vinci's house, Jason loves seeing creativity rewarded and new businesses succeed.</p>