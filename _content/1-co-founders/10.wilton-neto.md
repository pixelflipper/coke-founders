---
title: Wilton Neto
photo: /assets/images/wilton_neto.png
twitter: @wilton_pinheiro
linkedin: https://www.linkedin.com/in/wiltonpinheiro
startup: /startups/winnin
quote: Study hard to be someone in life.
---
<p>Wilton co-founded the private social network company B2Learn in 2008 and lead the company as CEO until it was sold in 2011. Also in 2011 he co-founded ViajeMe, a startup to inspire leisure travelers in Brazil. From 2006 until 2008 he worked in ABN AMRO headquarters as a PMO for global projects and M&A. Wilton is a business-driven Computer Engineer who graduated from Unicamp. He has lived in Amsterdam and 4 different cities in Brazil, and is currently based in Rio de Janeiro. He loves to cook, travel and run.</p>