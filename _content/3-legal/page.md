---
_fieldset: page
title: Legal Notices and Disclaimers
_template: static_page
---
<p>Coca-Cola owns the trademarks “Coca-Cola”, “Coke”, "Dynamic Ribbon" and all associated Coca-Cola trade names, trade dress, service marks and logos.  All other trademarks used on the Site are the property of their respective owners.  Coca-Cola trademarks and Coca-Cola logos may only be used in conjunction with goods produced by Coca-Cola or with the express prior approval of Coca-Cola. All rights reserved. For the avoidance of doubt, the Coca-Cola corporate logo may only be used by Coca-Cola.</p><p>This is not a solicitation for or offer of investment in any startup referenced on this website. Each startup featured on this website is owned and controlled by the startup’s co-founders and is not owned and controlled by The Coca‑Cola Company</p>